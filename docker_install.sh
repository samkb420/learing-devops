#!/bin/bash



# Tasks 1
    # Get Update.

sudo apt-get update

# Task 2 
    # Set up the repository

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

#Task 3
    # Add Docker’s official GPG key


curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


# Task 4
    # Use the following command to set up the stable repository

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# Task 5
    # Install Docker Engine

sudo apt-get update

sudo apt-get install  docker.io



# Task 6
    # Check if docker is installed

sudo docker --version




